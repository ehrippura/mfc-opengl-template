#pragma once

#include "stdafx.h"

class GLMainWindow;

class GLApp : public CWinApp {
public:
	GLApp();
	virtual ~GLApp();

protected:
	virtual BOOL InitInstance();

private:
	GLMainWindow *mainWindow;
};

