#pragma once

#define WINVER _WIN32_WINNT_WIN8
#define GL_GLEXT_PROTOTYPES

#include <afxwin.h>
#include <afxext.h>

#include <wingdi.h>
#include <glcorearb.h>