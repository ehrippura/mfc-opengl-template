#pragma once

#include "stdafx.h"

class GLAppResponder {
public:
	GLAppResponder();
	~GLAppResponder();

	void setWindow(HWND hWnd);

	// handle window message
public:
	void onWindowPaint() const;
	void onWindowResize() const;

protected:
	void initEnviornment() const;

private:
	HWND window;
	HDC deviceContext;

	void *vertexBuffer;
};

