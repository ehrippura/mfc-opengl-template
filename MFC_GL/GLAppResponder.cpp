#include "stdafx.h"
#include "GLAppResponder.h"

GLAppResponder::GLAppResponder()
{
}


GLAppResponder::~GLAppResponder()
{
}

void GLAppResponder::setWindow(HWND hWnd)
{
	window = hWnd;
	deviceContext = GetDC(window);

	initEnviornment();
}

void GLAppResponder::initEnviornment() const
{
	PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,    //Flags
		PFD_TYPE_RGBA,            //The kind of framebuffer. RGBA or palette.
		32,                        //Colordepth of the framebuffer.
		0, 0, 0, 0, 0, 0,
		0,
		0,
		0,
		0, 0, 0, 0,
		24,                        //Number of bits for the depthbuffer
		8,                        //Number of bits for the stencilbuffer
		0,                        //Number of Aux buffers in the framebuffer.
		PFD_MAIN_PLANE,
		0,
		0, 0, 0
	};

	int  letWindowsChooseThisPixelFormat;
	letWindowsChooseThisPixelFormat = ChoosePixelFormat(deviceContext, &pfd);
	SetPixelFormat(deviceContext, letWindowsChooseThisPixelFormat, &pfd);

	HGLRC ourOpenGLRenderingContext = wglCreateContext(deviceContext);
	wglMakeCurrent(deviceContext, ourOpenGLRenderingContext);

	// set opengl default environment
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
}

void GLAppResponder::onWindowPaint() const
{
	glClear(GL_COLOR_BUFFER_BIT);

	SwapBuffers(deviceContext);
}

void GLAppResponder::onWindowResize() const
{
	RECT windowRect;
	GetWindowRect(window, &windowRect);

	glFinish();
	glViewport(0, 0, windowRect.right - windowRect.left, windowRect.bottom - windowRect.top);
}


