#include "GLApp.h"
#include "GLMainWindow.h"

GLApp::GLApp()
{
}


GLApp::~GLApp()
{
}

BOOL GLApp::InitInstance()
{
	CWinApp::InitInstance();

	mainWindow = new GLMainWindow();
	m_pMainWnd = mainWindow;

	mainWindow->ShowWindow(m_nCmdShow);
	mainWindow->UpdateWindow();

	return TRUE;
}

