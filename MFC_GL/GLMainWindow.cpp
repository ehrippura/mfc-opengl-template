#include "GLMainWindow.h"
#include "GLAppResponder.h"

static const int windowSizeWidth = 800;
static const int windowSizeHeight = 600;

BEGIN_MESSAGE_MAP(GLMainWindow, CFrameWnd)
	ON_WM_PAINT()
END_MESSAGE_MAP()

GLMainWindow::GLMainWindow()
{
	int screenWidth = ::GetSystemMetrics(SM_CXSCREEN);
	int screenHeight = ::GetSystemMetrics(SM_CYSCREEN);

	CRect rect(
		screenWidth / 2 - windowSizeWidth / 2, screenHeight / 2 - windowSizeHeight / 2,
		screenWidth / 2 + windowSizeWidth / 2, screenHeight / 2 + windowSizeHeight / 2
	);

	DWORD style = WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX;
	Create(NULL, L"Hello OpenGL Context", style, rect);

	renderer = new GLAppResponder;
	renderer->setWindow(m_hWnd);

	timer = ::timeSetEvent(1000 / 60, 1, &GLMainWindow::timerCallback, (DWORD_PTR)this, TIME_PERIODIC);
}

GLMainWindow::~GLMainWindow()
{
	delete renderer;
	::timeKillEvent(timer);
}

void GLMainWindow::OnPaint()
{
	renderer->onWindowPaint();
}

void GLMainWindow::timerCallback(UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2)
{
	GLMainWindow *mainWindow = (GLMainWindow *)dwUser;
	mainWindow->OnPaint();
}
