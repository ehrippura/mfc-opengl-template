#pragma once
#include "stdafx.h"
#include <Mmsystem.h>

class GLAppResponder;

class GLMainWindow : public CFrameWnd {

public:
	GLMainWindow();
	virtual ~GLMainWindow();

	DECLARE_MESSAGE_MAP()

protected:
	afx_msg void OnPaint();
	static void CALLBACK timerCallback(UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2);

private:
	GLAppResponder *renderer;
	MMRESULT timer;
};

